.PHONY: kafka-shell
exec : kafka-shell
	docker exec -ti kafka-docker_kafka_1 /bin/bash

.PHONY: create-topic
create-topic :
	docker exec kafka-docker_kafka_1 /opt/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic test2

.PHONY: list-topic
list-topic :
	docker exec kafka-docker_kafka_1 /opt/kafka/bin/kafka-topics.sh --bootstrap-server localhost:9092 --list

.PHONY: list-messages-topic
list-messages-topic :
	docker exec kafka-docker_kafka_1 /opt/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic epub_imported --from-beginning