package com.franroa.translator.kafka;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class ImportedEpubSubmitterNonStaticConfig {
    @Value("${spring.kafka.template.epub_topic}")
    private String topic;

    @Bean
    public ImportedEpubSubmitterNonStatic importedEpubSubmitterNonStatic() {
        return new ImportedEpubSubmitterNonStatic();
    }
}
