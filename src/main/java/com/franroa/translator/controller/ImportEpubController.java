package com.franroa.translator.controller;

import com.franroa.translator.dtos.FileResponse;
import com.franroa.translator.kafka.ImportedEpubSubmitterNonStatic;
import com.franroa.translator.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


@RestController
@RequestMapping("/v1/translator")
public class ImportEpubController {
    private StorageService storageService;
    private ImportedEpubSubmitterNonStatic submitter;

    @Autowired
    public ImportEpubController(StorageService storageService, ImportedEpubSubmitterNonStatic submitter) {
        this.storageService = storageService;
        this.submitter = submitter;
    }

    @ResponseBody
    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = "application/json")
    public FileResponse store(@RequestParam("epub") MultipartFile epub) {
        String name = storageService.store(epub);

        String uri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/download/")
                .path(name)
                .toUriString();

        submitter.sendMessage(name);

        return new FileResponse(name, uri, epub.getContentType(), epub.getSize());
    }
}
