package com.franroa.translator.controller;

import com.franroa.translator.TestCase;
import com.franroa.translator.dtos.FileResponse;
import com.franroa.translator.storage.StorageProperties;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.util.MultiValueMap;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singleton;
import static org.assertj.core.api.Assertions.assertThat;


@EmbeddedKafka(partitions = 1)
public class ImportEpubControllerTest extends TestCase {
    // TODO The Topic should be changeable (now is fixed to the properties file)
    public static final String TOPIC = "test_imported_epub";
    private Consumer<String, String> consumer;
    private String rootLocation;
    private EmbeddedKafkaBroker embeddedKafkaBroker;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public ImportEpubControllerTest(EmbeddedKafkaBroker broker, StorageProperties storageProperties) {
        this.embeddedKafkaBroker = broker;
        this.rootLocation = storageProperties.getLocation();
    }

    @Test
    public void nullKey() throws Exception {
        Files.deleteIfExists(Paths.get("./uploads/Der kleine Prinz - Antoine de Saint-Exupery.epub"));
        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("epub", new ClassPathResource("Der kleine Prinz - Antoine de Saint-Exupery.epub"));
        MultiValueMap<String, HttpEntity<?>> multipartBody = builder.build();
        Map<String, Object> configs = new HashMap<>(KafkaTestUtils.consumerProps("consumer", "false", embeddedKafkaBroker));
        consumer = new DefaultKafkaConsumerFactory<>(configs, new StringDeserializer(), new StringDeserializer()).createConsumer();
        consumer.subscribe(singleton(TOPIC));
        consumer.poll(0);

        Assumptions.assumeFalse(Files.exists(Paths.get(rootLocation + "/Der kleine Prinz - Antoine de Saint-Exupery.epub")));

        var result =  webClient.post()
                .uri("/v1/translator")
                .syncBody(multipartBody)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .returnResult(FileResponse.class);

        ConsumerRecord<String, String> singleRecord = KafkaTestUtils.getSingleRecord(consumer, TOPIC);

        var epubStoreResponse = new FileResponse(
                "Der kleine Prinz - Antoine de Saint-Exupery.epub",
                "http://localhost:8080/download/Der%20kleine%20Prinz%20-%20Antoine%20de%20Saint-Exupery.epub",
                "application/epub+zip",
                4106605
        );
        assertThat(new String(result.getResponseBodyContent())).isEqualTo(epubStoreResponse.toString());
        assertThat(singleRecord).isNotNull();
        assertThat(singleRecord.value()).isEqualTo("Der kleine Prinz - Antoine de Saint-Exupery.epub");
    }
}